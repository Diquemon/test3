import model._
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.io.Source


object main extends App {
  val db = slickProfile.api.Database.forURL("jdbc:postgresql://localhost/airport?user=postgres&password=postgres")
  val companyRepository = new companyRepository(db)
  val passengerRepository = new passengerRepository(db)
  val tripRepository = new tripRepository(db)
  val passInTripRepository = new passInTripRepository(db)

  def init(): Unit = {
    Await.result(db.run(companyRepository.table.schema.create), Duration.Inf)
    Await.result(db.run(passengerRepository.table.schema.create), Duration.Inf)
    Await.result(db.run(tripRepository.table.schema.create), Duration.Inf)
    Await.result(db.run(passInTripRepository.table.schema.create), Duration.Inf)
  }

  def fillTables(): Unit ={
    val format = new java.text.SimpleDateFormat("yyyyMMdd HH:mm:ss")
    for (line <- Source.fromFile("C:\\Users\\keppn\\IdeaProjects\\test3\\src\\main\\scala\\model\\passengers.txt").getLines){
      val temp = line.split("[,\\s]+")
      Await.result(passengerRepository.create(Passenger(Some(temp(0).toInt),temp(1)+" "+temp(2))), Duration.Inf)
    }
    for (line <- Source.fromFile("C:\\Users\\keppn\\IdeaProjects\\test3\\src\\main\\scala\\model\\companies.txt").getLines){
      val temp = line.split("[,\\s]+")
      Await.result(companyRepository.create(Company(Some(temp(0).toInt), temp(1))), Duration.Inf)
    }
    for (line <- Source.fromFile("C:\\Users\\keppn\\IdeaProjects\\test3\\src\\main\\scala\\model\\trips.txt").getLines){
      val temp = line.split("[,\\s]+")
      Await.result(tripRepository.create(Trip(Some(temp(0).toInt),
        temp(1).toInt,temp(2),temp(3), temp(4),
        format.parse(temp(5)+" "+temp(6)), format.parse(temp(7)+" "+temp(8)))), Duration.Inf)
    }
    for (line <- Source.fromFile("C:\\Users\\keppn\\IdeaProjects\\test3\\src\\main\\scala\\model\\passengersInTrips.txt").getLines) {
      val temp = line.split("[,\\s]+")
      Await.result(passInTripRepository.create(PassInTrip(temp(0).toInt, format.parse(temp(1)+" "+temp(2)), temp(3).toInt, temp(4))), Duration.Inf)
    }
  }
    def Task63(): Unit = {

      val query = passInTripRepository.table.join(passengerRepository.table).on(_.idp === _.id)
        .groupBy{case (pssngrInTrip, pssngr) => (pssngrInTrip.place, pssngrInTrip.idp, pssngr.name) }
        .map{case ((place, passId, name), group) => (group.size, name)}
        .filter(pssngr => pssngr._1 > 1).map(_._2).result
      Await.result(db.run(query), 15.seconds)
    }

    def Task67(): Unit = {
      val query = tripRepository.table.groupBy(trip => (trip.townFrom, trip.townTo))
        .map{ case ((townFrom, townTo), group) => (group.length, townFrom, townTo)}
        .sortBy{ case (count, townFrom, townTo) => count }
        .take(1)
      Await.result(db.run(query.length.result), 15.seconds)
    }

  def Task68(): Unit = {
    val sub1 = (tripRepository.table.map(t => (t.id, t.townFrom, t.townTo)) unionAll tripRepository.table.map(t => (t.id, t.townTo, t.townFrom)))
      .groupBy { case t => (t._2, t._3) }
      .map{ case (_, group) => group.size }

    val sub2 = sub1.groupBy {_=>true}.map{case (_, group) => group.max}

    val query = sub1.filter {_ in sub2}.length
    Await.result(db.run(query.result), 15.seconds)/2
  }

    def Task72 (): Unit = {
      val sub1 = passInTripRepository.table.join(tripRepository.table).on(_.idt === _.id)
        .groupBy{ case (psgInTrip, psg) => (psgInTrip.idp, psg.idc) }
        .map{ case ((idp, idc), group) => (idp, idc, group.size) }

      val sub2 = sub1.map { case (idp, idc, size) => size }
        .groupBy { _ => true }.map { case (_, group) => group.max }

      val query = (sub1.filter { case line => line._3 in sub2 }
        .map { case line => (line._1, line._3) })
        .join(passengerRepository.table).on(_._1 === _.id)
        .map { case (pmax, psg) => (psg.name, pmax._2) }

      Await.result(db.run(query.result), 15.seconds) //.foreach(println)
    }

    def Task77(): Unit = {
      val query = tripRepository.table.filter(_.townFrom === "Rostov")
        .join(passInTripRepository.table).on(_.id === _.idt)
        .map{case (trip, psgInTrip) => (psgInTrip.idt, psgInTrip.date)}
        .groupBy{case (id, date) => date}
        .map{case (date, group) => (date, group.size)}
        .result
      Await.result(db.run(query), 15.seconds)
    }
  /*
    def Task88(): Unit = {
      val query = passengerRepository.table.join(passInTripRepository.table).on(_.id === _.idp)
        .join(tripRepository.table).on(_._2.idt === _.id)
        .join(companyRepository.table).on(_._2.idc === _.id)
        .groupBy(
    }*/
  def Task114(): Unit = {
    val temp = passInTripRepository.table.join(passengerRepository.table).on(_.idp === _.id)
      .map{case (pit,p) => (p.name, pit.place)}
      .groupBy{case (name, place) => (name, place)}
      .map{case ((name,place), group) => (name, group.size)}
    val query = temp.filter{case (name, times) => times in temp.map(_._2)
      .groupBy { _ => true }
      .map { case (_, group) => group.max }}
    Await.result(db.run(query.result), 15.seconds)
  }
    def Task95(): Unit = {
      val query = (for {
        comp <- companyRepository.table
        trip <- tripRepository.table if comp.id ===trip.idc
        psgInTrip <- passInTripRepository.table if trip.id === psgInTrip.trip_id_fk
      } yield (comp, trip, psgInTrip))
        .groupBy { case (comp, trip, psgInTrip) => (comp.id, comp.name) }
        .map { case ((comp, trip, pit), group) => (
          group.map(_._1.name).countDistinct,
          group.map(_._2.id).countDistinct,
          group.map(_._2.plane).countDistinct,
          group.map(_._3.pass_id_fk).countDistinct,
          group.length)}
        .result
      Await.result(db.run(query), 15.seconds)
    }
    def Task94(): Unit = {
      val format = new java.text.SimpleDateFormat("yyyyMMdd HH:mm:ss")
      val query = passInTripRepository.table.join(tripRepository.table).on(_.idt === _.id)
        .filter(_._2.townFrom === "Rostov")
        .filter(_._1.date < format.parse("20030407 00:00:00"))
        .groupBy(trip => (trip._1.date, trip._2.townFrom))
        .map { case ((date, trip), count) => (count.countDistinct, date) }
      Await.result(db.run(query.result), 20.seconds)
    }

    def Task103(): Unit = {
      val query = (for {
        trip1 <- tripRepository.table
        trip2 <- tripRepository.table
        trip3 <- tripRepository.table
      } yield (trip1.id, trip2.id, trip3.id))
        .filter(trip => trip._2 > trip._1 && trip._3 > trip._2)
        .groupBy { _ => true }
        .map { case (_, group) => (group.map(_._1).min, group.map(_._2).min, group.map(_._3).min,
          group.map(_._1).max, group.map(_._2).max, group.map(_._3).max)
        }
      Await.result(db.run(query.result), 15.seconds)
    }

    def Task87(): Unit = {
      val query = passengerRepository.table.join(passInTripRepository.table).on(_.id === _.idp)
        .join(tripRepository.table).on(_._2.idt === _.id)
        .filter(trip => trip._2.townTo === "Moscow")
        .groupBy(trip => (trip._1._1.name, trip._2.townTo, trip._2.townFrom))
        .map { case ((name, to, from), count) => (name, count.length) }
        .filter { case (name, count) => count > 1 }
      Await.result(db.run(query.result), 20.seconds)
    }

    def Task122(): Unit = {
      val tempSubQ = passInTripRepository.table.join(tripRepository.table).on(_.idt === _.id)
        .join(passengerRepository.table).on{case ((psgInTrip, trip), psg) => psgInTrip.idp === psg.id}
        .map{case ((psgInTrip, trip), psg) => (psg.id, psg.name, psgInTrip.date, trip.townFrom, trip.townTo)}
      val idOfResidence = tempSubQ
        .groupBy{case (idp, name, date, from , to) => idp}
        .map{case (idp, group) => (idp, group.map(_._3).min)}
        .join(tempSubQ).on(_._2 === _._3)
        .map{case (firstTrip, trips) => ( trips._1, trips._4)}.distinct
        .distinctOn{case (id, town) => id}
      val idOfLastTown = tempSubQ
        .groupBy{case (idp, name, date, from, to) => idp}
        .map{case (idp, group) => (idp, group.map(_._3).max)}
        .join(tempSubQ).on{case (p, group) => (p._1 === group._1)&&(p._2 === group._3)}
        .map{case (lastTrip, trips) => ( trips._1, trips._2, trips._5)}.distinct
        .distinctOn{case (id, name,town) => id}
      val query = idOfLastTown.join(idOfResidence).on(_._1 === _._1)
        .map{case (lastTrip, residence) => (lastTrip._2, residence._2, lastTrip._3)}
        .filter{case (name, residence, lastTown) => residence =!= lastTown}
      Await.result(db.run(query.result), 15.seconds)
    }
    def Task102(): Unit = {
      val query = for{
        idPass<-passengerRepository.table
        trip<-passInTripRepository.table.filter(_.idp===idPass.id).map(_.idt)
        towns<-tripRepository.table.filter(_.id===trip).map(x=>(x.townFrom,x.townTo))
      }yield (idPass.name,towns)
      Await.result(db.run(query.result),15.seconds)
    }

}
