package model
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.Future

case class Company (id: Option[Int], name: String)

class companyTable(tag:Tag) extends Table[Company](tag, "companies"){
  val id = column[Int]("id", O.PrimaryKey)
  val name = column[String]("name")
  def * = (id.?,  name) <> (Company.apply _ tupled, Company.unapply)
}

object companyTable {
  val table = TableQuery[companyTable]
}

class companyRepository(db:Database){
  val table = TableQuery[companyTable]

  def create(company: Company): Future[Company] =  db.run(companyTable.table returning companyTable.table += company)
  def update (company: Company): Future[Int] = db.run(companyTable.table.filter(_.id === company.id).update(company))
  def delete (companyId: Int): Future[Int] = db.run(companyTable.table.filter(_.id === companyId).delete)
  def getById(companyId: Int): Future[Option[Company]] = db.run(companyTable.table.filter(_.id === companyId).result.headOption)
}