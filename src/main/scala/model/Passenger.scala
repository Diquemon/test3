package model
import slick.jdbc.PostgresProfile.api._
import slick.lifted.TableQuery
import scala.concurrent.Future

case class Passenger (id: Option[Int], name: String)

class passengerTable(tag:Tag) extends Table[Passenger](tag, "passengers"){
  val id = column[Int]("id_psg", O.PrimaryKey)
  val name = column[String]("name")
  def * = (id.?,  name) <> (Passenger.apply _ tupled, Passenger.unapply)
}

object passengerTable {
  val table = TableQuery[passengerTable]
}

class passengerRepository(db:Database){
  val table = TableQuery[passengerTable]

  def create(passenger: Passenger): Future[Passenger] = db.run(passengerTable.table returning passengerTable.table += passenger)
  def update (passenger: Passenger): Future[Int] = db.run(passengerTable.table.filter(_.id === passenger.id).update(passenger))
  def delete (passengerId: Int): Future[Int] = db.run(passengerTable.table.filter(_.id === passengerId).delete)
  def getById(passengerId: Int): Future[Option[Passenger]] = db.run(passengerTable.table.filter(_.id === passengerId).result.headOption)
}