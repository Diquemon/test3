import slick.jdbc.PostgresProfile.api._
import slick.ast.BaseTypedType


package object model {
  implicit val DateToSqlTimestampMapper: BaseTypedType[java.util.Date] = MappedColumnType.base[java.util.Date, java.sql.Timestamp](
    date => new java.sql.Timestamp(date.getTime()),
    timestamp => new java.util.Date(timestamp.getTime()))
}