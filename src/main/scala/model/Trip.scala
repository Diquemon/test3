package model
import slick.jdbc.PostgresProfile.api._
import java.util.Date
import scala.concurrent.Future

case class Trip (id: Option[Int], idc: Int, plane: String, townFrom: String, townTo: String, timeIn: Date, timeOut: Date)

class tripTable(tag:Tag) extends Table[Trip](tag, "trips"){
  val id = column[Int]("trip_no", O.PrimaryKey)
  val idc = column[ Int]("ID_comp")
  val plane = column[String]("plane")
  val townFrom = column[ String]("town_from")
  val townTo = column[String]("town_to")
  val timeIn = column[Date]("time_in")
  val timeOut = column[Date]("time_out")
  def * = (id.?,  idc,  plane,  townFrom,  townTo,  timeIn,  timeOut) <> (Trip.apply _ tupled, Trip.unapply)

}

object tripTable {
  val table = TableQuery[tripTable]
}

class tripRepository(db:Database){
  val table = TableQuery[tripTable]
  def create(trip: Trip): Future[Trip] = db.run(tripTable.table returning tripTable.table += trip)
  def update (trip: Trip): Future[Int] = db.run(tripTable.table.filter(_.id === trip.id).update(trip))
  def delete (idt: Int): Future[Int] = db.run(tripTable.table.filter(_.id === idt).delete)
  def getById(idt: Int): Future[Option[Trip]] = db.run(tripTable.table.filter(_.id === idt).result.headOption)
}