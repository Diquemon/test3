package model
import java.util.Date
import slick.jdbc.PostgresProfile.api._
import slick.lifted.{PrimaryKey, TableQuery}
import scala.concurrent.Future

case class PassInTrip(idt: Int,  date: Date, idp : Int, place: String)

class passInTripTable(tag:Tag) extends Table[PassInTrip](tag, "pass_in_trip"){
  val idt = column[Int]("trip_no")
  val date = column[Date]("[date]")
  val idp = column[Int]("id_psg")
  val place = column[String]("place")
  val trip_id_fk = foreignKey("trip_no_fk", idt, TableQuery[tripTable])(_.id)
  val pass_id_fk = foreignKey("id_psg_fk", idp, TableQuery[passengerTable])(_.id)
  val pk = primaryKey("pass_in_trip_pk", (idt, idp, date))
  def * = (idt,  date, idp, place) <> (PassInTrip.apply _ tupled, PassInTrip.unapply)
}

object passInTripTable {
  val table = TableQuery[passInTripTable]
}

class passInTripRepository(db:Database){
  val table = TableQuery[passInTripTable]

  def create(passInTrip: PassInTrip): Future[PassInTrip] = db.run(passInTripTable.table returning passInTripTable.table += passInTrip)
  def update (passInTrip: PassInTrip): Future[Int] = db.run(passInTripTable.table.filter(pass => (
    pass.idt === passInTrip.idt) && (pass.idp === passInTrip.idp) && (pass.date === passInTrip.date)).update(passInTrip))
  def delete (idt:Int, idp:Int, date:Date ): Future[Int] = db.run(passInTripTable.table.filter(pass =>
    (pass.idt === idt) && (pass.idp === idp) && (pass.date === date)).delete)
  def getByDate(idt:Int, idp:Int, date:Date): Future[Option[PassInTrip]] = db.run(passInTripTable.table.filter(pass =>
    (pass.idt === idt) && (pass.idp === idp) && (pass.date === date)).result.headOption)
}
